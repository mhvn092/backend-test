import { createConnection } from "typeorm";
import express from "express";
import { DoctorEntity } from "./entities/doctor-entity";
import { UserEntity } from "./entities/user-entity";
import { TicketEntity } from "./entities/ticket-entity";
import { TicketController } from "./routes/ticket.controller";

const app = express();

async function main() {
  try {
    await createConnection({
      type: 'mysql',
      host: "localhost",
      port: 3306,
      username: "test",
      password: "123@test",
      extra: {
        trustServerCertificate: true,
      },
      database: "typeorm",
      synchronize: true,
      entities: [DoctorEntity,UserEntity,TicketEntity],
    });

    console.log("database connected");
    app.use(express.json());
    app.use("/api/ticket/",TicketController);

    app.listen(3000, () => console.log("Listening on port 3000"));
  } catch (e: any) {
    console.error(e);
    console.log("connection error");
  }
}

main();
