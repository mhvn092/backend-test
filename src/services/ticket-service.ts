import { TicketEntity } from "../entities/ticket-entity";

export class TicketService {
    public async insert(data: TicketEntity) {
        const exists = await TicketEntity.findOne({where:{
          userId:data.userId,doctorId:data.doctorId
        },relations:['relatedDoctor']} 
        );
        if (exists){
          throw new Error('user already has ticket')
        }
        const doctorTicketCount = await TicketEntity.count({where:{doctorId:data.doctorId}});
        if(doctorTicketCount >= exists.relatedDoctor.maxTicketCount){
          throw new Error('maximum ticket for doctor')
        }
        
        return TicketEntity.insert(data)
      }
}