import express from "express";
import { TicketEntity } from "../entities/ticket-entity";
import { TicketService } from "../services/ticket-service";

const router = express.Router();

const ticketService = new TicketService();
router.post("/getTicket/", async (req: { body: { userId: number; }; }, res: any ) => {
  const { userId } = req.body;
  if(!isNaN(userId)){
    throw new Error('id invalid');
  }

  const ticket = new TicketEntity();
  ticket.doctorId = 1;
  ticket.userId = userId
  await ticketService.insert(ticket);

  return res.json(ticket);
});



export { router as TicketController };
