import { BaseEntity, Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { TicketEntity } from "./ticket-entity";

@Entity('User')
export class UserEntity extends BaseEntity{
@PrimaryGeneratedColumn({type:"int"})
id:number;

@Column({type:'varchar'})
name:string;

@OneToMany(()=>TicketEntity, (ticket)=>ticket.relatedUser)
tickets: TicketEntity[];

}