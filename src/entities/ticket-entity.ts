import { BaseEntity, Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { DoctorEntity } from "./doctor-entity";
import { UserEntity } from "./user-entity";


@Entity('Ticket')
export class TicketEntity extends BaseEntity{
@PrimaryGeneratedColumn({type:"int"})
id:number;

@Column({type:"int"})
userId:number;

@Column({type:"int"})
doctorId:number;

@ManyToOne(()=>UserEntity,(user)=>user.tickets)
relatedUser:UserEntity;

@ManyToOne(()=>DoctorEntity,(doctor)=>doctor.tickets)
relatedDoctor:DoctorEntity;
}