import { BaseEntity, Column, Entity, JoinColumn, JoinTable, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { TicketEntity } from "./ticket-entity";

@Entity('Doctor')
export class DoctorEntity extends BaseEntity {
@PrimaryGeneratedColumn ({type:"int"})
id:number;

@Column({type:'varchar'})
name:string;

@Column({
    type:'int',
    default: 10
})
maxTicketCount: number;

@OneToMany(()=>TicketEntity, (ticket)=>ticket.relatedDoctor)
tickets: TicketEntity[];


}